package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Implementation.Display";
    public static String controlPanelImplClassName = "pk.labs.LabA.Implementation.ControlPanel";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.MainComponentSpec";
    public static String mainComponentImplClassName = "pk.labs.LabA.Implementation.MainComponent";
    public static String mainComponentBeanName = "main";
    // endregion

    // region P2
    public static String mainComponentMethodName = "...";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "...";
    // endregion
}
